# Bizmandu Backend Project
(Express, Mysql, Nodemon, Sequelize)

#Clone Project  

    git clone https://subodhPradhan@bitbucket.org/subodhPradhan/bizmandu-backend.git

#Install Packages

    sudo npm install
    
#Run Project

    npm run start
    
#Build Project For Production

    npm run build

Author

Subodh Pradhan
