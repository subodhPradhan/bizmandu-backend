const log4js = require('log4js')
const Utils = {
  getCorrespondingIndicesName: function (indicesName) {
    switch (indicesName) {
      case 'nepse': return 'Nepse'
      case 'sensitive': return 'Sensitive'
      case 'float': return 'Float'
      case 'senFloat': return 'Sen. Float'
      case 'banking': return 'Banking'
      case 'devBank': return 'Development Bank'
      case 'hydropower': return 'Hydro Power'
      case 'lifeInsurance': return 'Life Insurance'
      case 'nonLifeInsurance': return 'Non Life Insurance'
      case 'finance': return 'Finance'
      case 'others': return 'Others'
      default : return indicesName
    }
  },
  getLogger: function (logType) {
    // Log Types (debug, trace, info, warn, error, fatal)
    log4js.configure({
      appenders: {
        out: {type: 'console'},
        default: {type: 'file', filename: `logs/${new Date().toLocaleString().split(',')[0].replace(new RegExp('/', 'g'), '-')}_default.log`},
        error: {type: 'file', filename: `logs/${new Date().toLocaleString().split(',')[0].replace(new RegExp('/', 'g'), '-')}_error.log`}
      },
      categories: {
        error: { appenders: [ 'out', 'error' ], level: 'error' }, // info, debug, error
        default: { appenders: [ 'out', 'default' ], level: 'info' }
      }
    })
    return log4js.getLogger(logType)
  },
  logType: {
    ERROR: 'error',
    DEFAULT: 'default'
  },
  removeNull: function (response) {
    if (typeof response === 'object') {
      for (let key in response) {
        if (response[key] === null || response[key] === 'null' || response[key] === undefined) {
          delete response[key]
        }
      }
    } else {
      response.forEach((row) => {
        for (let key in row) {
          if (row[key] === null || row[key] === 'null' || row[key] === undefined) {
            delete row[key]
          }
        }
      })
    }
    return response
  },
  response (code, title, response, errorMsg, res) {
    if (code === 200) {
      this.getLogger(this.logType.DEFAULT).info(`[${title}] -- Success`)
    } else if (code !== 404) {
      this.getLogger(this.logType.ERROR).error(`[${title}] -- Failure \n ${errorMsg} \n\n`)
    }
    if (res !== undefined) { return res.json({response: code, error: errorMsg, message: response}) }
  }
}
module.exports = Utils
