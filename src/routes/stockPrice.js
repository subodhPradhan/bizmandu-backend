/**
 * Created by admin to 2018/11/4.
 */
const router = require('express').Router()
const {StockPriceService} = require('../services')
const Utils = require('../common/util')

router.get('/all', (req, res, next) => {
  StockPriceService.StockPrice.getAll((err, result) => {
    if (!err) {
      if (result.length > 0) {
        result = result.map((row) =>
          (Utils.removeNull({
            ticker: row.ticker,
            ltp: row.ltp,
            ltv: row.ltv,
            pointChange: row.pointChange,
            percentageChange: row.percentageChange,
            open: row.open,
            high: row.high,
            low: row.low,
            volume: row.volume
          }))
        )
        Utils.response(200, 'Stock Price', result, '', res)
      }
    } else {
      Utils.response(500, 'Stock Price', [], 'Internal Server Error', res)
    }
  })
})

router.get('/percentageChange/all', (req, res, next) => {
  StockPriceService.StockPrice.getAllPriceList((err, result) => {
    if (!err) {
      Utils.response(200, 'Stock Price Percentage Change', Utils.removeNull(result), '', res)
    } else {
      Utils.response(500, 'Stock Price Percentage Change', [], 'Internal Server Error', res)
    }
  })
})

module.exports = router
