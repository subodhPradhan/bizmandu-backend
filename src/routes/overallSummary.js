/**
 * Created by admin to 2018/11/4.
 */
const router = require('express').Router()
const {MarketOverviewService, ForexSummaryService} = require('../services')
const Utils = require('../common/util')

router.get('/all', (req, res, next) => {
  MarketOverviewService.IndicesSummary.getAll((err, Indices) => {
    if (!err) {
      Indices = Indices.filter((i) => i.indices === 'nepse' || i.indices === 'sensitive' || i.indices === 'float' || i.indices === 'senFloat').map((row) =>
        Utils.removeNull({
          indicesName: row.indices,
          title: Utils.getCorrespondingIndicesName(row.indices),
          current: row.current,
          pointChange: row.percentageChange > 0 ? row.pointChange : -1 * row.pointChange,
          percentageChange: row.percentageChange
        })
      )

      ForexSummaryService.getAll((err, forex) => {
        if (!err) {
          forex = forex.map((row) => Utils.removeNull(row))
          Utils.response(200, 'Market Overview Indices', Indices.concat(forex), '', res)
        } else {
          Utils.response(500, 'Market Overview Indices', [], 'Internal Server Error', res)
        }
      })
    } else {
      Utils.response(500, 'Market Overview Indices', [], 'Internal Server Error', res)
    }
  })
})

module.exports = router
