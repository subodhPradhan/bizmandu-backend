/**
 * Created by admin to 2018/11/4.
 */
const router = require('express').Router()
const {AnnouncementService} = require('../services')
const Utils = require('../common/util')

router.get('/announcement', (req, res, next) => {
  if (req.query['type'] !== null && req.query['type'] !== undefined) {
    AnnouncementService.Announcement.getAll(req.query['type'], (err, result) => {
      if (!err) {
        if (result.length > 0) {
          result = result.map((row) =>
            (Utils.removeNull({
              id: row.id,
              ticker: row.ticker,
              company: row.companyName,
              inputDate: row.inputDate,
              eventDate: row.eventDate,
              fiscalYear: row.fiscalYear,
              details: row.details,
              agenda: row.agenda,
              openDate: row.openDate,
              closeDate: row.closeDate,
              bookCloseDate: row.bookCloseDate,
              type: row.announcementType,
              allotmentDate: row.allotmentDate,
              shareRegistrar: row.shareRegistrar,
              venue: row.venue,
              time: row.time
            })))
        }
        Utils.response(200, 'Announcement', result, '', res)
      } else {
        Utils.response(500, 'Announcement', [], 'Internal Server Error', res)
      }
    })
  } else {
    Utils.response(500, 'Announcement', [], 'Query parameter not present.', res)
  }
})

router.get('/summary', (req, res, next) => {
  AnnouncementService.Announcement.getSummary((req.query['count'] !== null && req.query['count'] !== undefined) ? req.query['count'] : 20, (err, result) => {
    if (!err) {
      if (result.length > 0) {
        result = result.map((row) =>
          (Utils.removeNull({
            id: row.id,
            ticker: row.ticker,
            company: row.companyName,
            inputDate: row.inputDate,
            eventDate: row.eventDate,
            fiscalYear: row.fiscalYear,
            details: row.details,
            agenda: row.agenda,
            openDate: row.openDate,
            closeDate: row.closeDate,
            bookCloseDate: row.bookCloseDate,
            type: row.announcementType,
            allotmentDate: row.allotmentDate,
            shareRegistrar: row.shareRegistrar,
            venue: row.venue,
            time: row.time
          })))
      }
      Utils.response(200, 'Announcement Summary', result, '', res)
    } else {
      Utils.response(500, 'Announcement Summary', [], 'Internal Server Error', res)
    }
  })
})

router.get('/todaysEvent', (req, res, next) => {
  AnnouncementService.TodaysEvent.getAll((err, result) => {
    if (!err) {
      if (result.length > 0) {
        result = result.map((row) =>
          (Utils.removeNull({
            id: row.id,
            ticker: row.ticker,
            company: row.company,
            inputDate: row.inputDate,
            type: row.eventType,
            fiscalYear: row.fiscalYear,
            details: row.details,
            shareRegistrar: row.shareRegistrar,
            eventDate: row.eventDate,
            openDate: row.openDate,
            closeDate: row.closeDate,
            bookCloseDate: row.bookCloseDate,
            allotmentDate: row.allotmentDate,
            venue: row.venue,
            agenda: row.agenda,
            time: row.time
          })))
      }
      Utils.response(200, 'Announcement Today\'s Event', result, '', res)
    } else {
      Utils.response(500, 'Announcement Today\'s Event', [], 'Internal Server Error', res)
    }
  })
})

module.exports = router
