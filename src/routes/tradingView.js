const router = require('express').Router()
var https = require('https');
const dateformat = require('dateformat')
var {Tickers,StockData,LatestPriceLive,IndicesNames,IndicesData,IndicesDataOneDayLive} = require('../models')
const Sequelize = require('sequelize')
//Data feed configuration data
var smartSearch = require('smart-search')
const Op = Sequelize.Op;
router.get('/test', (req, res, next) => {

  const fromDate = new Date("1543137300000")
  const toDate = new Date("1543137300000")
  console.log(fromDate.getHours()+":"+toDate.getMinutes())
  // StockData.findAll({
  //   where: {
  //     ticker: 'NABIL',
  //     tradingDate: {$between:[fromDate,toDate]}
  //   }
  // }).then(data => {
  //   res.send(JSON.stringify(buildDataForTradingView(data)))
  // })

  // LatestPriceLive.find({
  //   where:{
  //     ticker:'NABIL',
  //     timestamp:{$lte:toDate}
  //   }
  // }).then(data=>{
  //   console.log(data)
  //     res.send(JSON.stringify(data.get()))
  // })
  // .catch(()=>{
  //   res.send("No Data")
  // })
    IndicesNames.findAll().then(data=>{
      console.log(data)
      res.send("Ok")
    })

})

router.get('/config', (req, res, next) => {
  const data = {
    supports_search: true,
    supports_group_request: false,
    supports_marks: false,
    supports_timescale_marks: false,
    supports_time: true,
    exchanges: [
      {
        value: '',
        name: 'All Exchanges',
        desc: ''
      },
      {
        value: 'NEPSE',
        name: 'NEPSE',
        desc: 'NEPSE'
      }
      // {
      //   value: 'NYSE',
      //   name: 'NYSE',
      //   desc: 'NYSE'
      // },
      // {
      //   value: 'NCM',
      //   name: 'NCM',
      //   desc: 'NCM'
      // },
      // {
      //   value: 'NGM',
      //   name: 'NGM',
      //   desc: 'NGM'
      // }
    ],
    symbols_types: [
      {
        name: 'All types',
        value: ''
      },
      {
        name: 'Stock',
        value: 'stock'
      },
      {
        name: 'Index',
        value: 'index'
      }
    ],
    // supported_resolutions: ['5','15','30','60','1D','1W','1M','6M']
    supported_resolutions: ['1D','1W','1M','6M']
  };
  res.send(JSON.stringify(data))
})

//Symbol resolve
router.get('/symbols', (req, res, next) => {
  const symbol = req.query.symbol

  const indexSymbols = IndicesNames.findAll(
   ).then(data=>{
    const entries = getIndexEntries(data)
    const result = entries.filter(k=>(k.symbol==symbol) || (k.full_name==symbol))
    return result[0]
  })

  const stockSymbol = Tickers.find({
    where: {
      [Op.or]:[{ticker:symbol},{companyName:symbol}]
    }
  }).then(data=>{
    return data
  })

  Promise.all([indexSymbols,stockSymbol]).then((combinedData)=>{
    console.log(combinedData)
    //!Array.isArray(combinedData[0]) || !combinedData[0].length
    if(combinedData[0]==null){
      console.log("No Index")
      const name=combinedData[1].get().ticker
      const description=combinedData[1].get().companyName

      const symbolData = {
        name: name,
        timezone: 'Asia/Kathmandu',
        minmov: 1,
        minmov2: 0,
        pointvalue: 1,
        session: '1100-1500:12345',
        has_intraday: true,
        has_no_volume: false,
        description: description,
        type: 'stock',
        //supported_resolutions: ['5','15','30','60','1D','1W','1M','6M'],
        supported_resolutions: ['1D','1W','1M','6M'],
        pricescale: 100,
        ticker: name
      }
      res.send(JSON.stringify(symbolData))
    }else if(combinedData[1]==null){
      console.log(combinedData[0])
      const name=combinedData[0].symbol
      const description=combinedData[0].full_name
      console.log("No Stock" +name+":"+description)
      const symbolData = {
        name: name,
        timezone: 'Asia/Kathmandu',
        minmov: 1,
        minmov2: 0,
        pointvalue: 1,
        session: '1100-1500:12345',
        has_intraday: true,
        has_no_volume: false,
        description: description,
        type: 'index',
        //supported_resolutions: ['5','15','30','60','1D','1W','1M','6M'],
        supported_resolutions: ['1D','1W','1M','6M'],
        pricescale: 100,
        ticker: name+"_"+"index"
      }
      res.send(JSON.stringify(symbolData))
    }
  }).catch(()=>{
    res.send("No Data")
  })

  // Tickers.find({
  //   where: {
  //     [Op.or]:[{ticker:symbol},{companyName:symbol}]
  //   }
  // }).then(data => {

  //   const symbolData = {
  //     name: data.get().ticker,
  //     timezone: 'Asia/Kathmandu',
  //     minmov: 1,
  //     minmov2: 0,
  //     pointvalue: 1,
  //     session: '1100-1500',
  //     has_intraday: true,
  //     has_no_volume: false,
  //     description: data.get().companyName,
  //     type: 'stock',
  //     supported_resolutions: [
  //       '5',
  //       '15',
  //       '30',
  //       '60',
  //       '1D',
  //       '1W',
  //       '1M',
  //       '6M'
  //     ],
  //     pricescale: 100,
  //     ticker: data.get().ticker
  //   }
  //   res.send(JSON.stringify(symbolData))
  // })


})

//Symbol search
router.get('/search', (req, res, next) => {
  const query = req.query.query
  const type = req.query.type
  const exchange = req.query.exchange
  const limit = req.query.limit

  if(type=="index"){
    IndicesNames.findAll().then(data=>{
      const entries = getIndexEntries(data)
      var patterns = [query]
      var fields = {symbol:true,full_name:true}
      var results = smartSearch(entries,patterns,fields,{fieldMatching:true,caseSensitive:true})
  
      const symbolData = results.map(k=>({
        symbol:k.entry.symbol,
        full_name:k.entry.full_name,
        description:k.entry.full_name,
        exchange:'NEPSE',
        type:'index'
      }))
      res.send(JSON.stringify(symbolData)) 
    }).catch(()=>{
      console.log("/search No Result for query "+query)
    })

  }else if(type=="stock"){
    Tickers.findAll({
    }).then(data => {
      const entries = getSymbolEntries(data)
      var patterns = [query]
      var fields = {symbol:true,full_name:true}
      var results = smartSearch(entries,patterns,fields,{fieldMatching:true,caseSensitive:true})
  
      const symbolData = results.map(k=>({
        symbol:k.entry.symbol,
        full_name:k.entry.full_name,
        description:k.entry.full_name,
        exchange:'NEPSE',
        type:'stock'
      }))
      res.send(JSON.stringify(symbolData))
    }).catch(()=>{
      console.log("/search No Result for query "+query)
    })
  }else{
   const stockSymbols= Tickers.findAll({
    }).then(data => {
      const entries = getSymbolEntries(data)
      var patterns = [query]
      var fields = {symbol:true,full_name:true}
      var results = smartSearch(entries,patterns,fields,{fieldMatching:true,caseSensitive:true})
  
      const symbolData = results.map(k=>({
        symbol:k.entry.symbol,
        full_name:k.entry.full_name,
        description:k.entry.full_name,
        exchange:'NEPSE',
        type:'stock'
      }))
      return symbolData
    })
   const indexSymbols = IndicesNames.findAll().then(data=>{
    const entries = getIndexEntries(data)
    var patterns = [query]
    var fields = {symbol:true,full_name:true}
    var results = smartSearch(entries,patterns,fields,{fieldMatching:true,caseSensitive:true})

    const symbolData = results.map(k=>({
      symbol:k.entry.symbol,
      full_name:k.entry.full_name,
      description:k.entry.full_name,
      exchange:'NEPSE',
      type:'index'
    }))
    return symbolData
  })
  Promise.all([stockSymbols,indexSymbols]).then((combinedData)=>{
    const allSymbols = combinedData[0].concat(combinedData[1])
    res.send(JSON.stringify(allSymbols))
  }).catch(()=>{
    console.log("/search No Result for query "+query)
  })

  }

})

function getSymbolEntries(data){
  const entries = data.map(row=>({symbol:row.dataValues.ticker,full_name:row.dataValues.companyName}))
  return entries
}

function getIndexEntries(data){
  //data.map(row=>console.log(row.dataValues))
  const entries = data.map(row=>({symbol:row.dataValues.indicesName.toUpperCase(),full_name:row.dataValues.indicesName}))
  return entries
}

function buildDataForLatest(dataArray){
  const data = dataArray[0]
  const tradingDate = new Date(data.timestamp)
  const year = tradingDate.getUTCFullYear()
  const month = tradingDate.getUTCMonth()
  const day= tradingDate.getUTCDate()
  const newDate = (Date.UTC(year,month,day,0,0,0,0)/1000)
  const closingPrice = data.closingPrice
  const openPrice = data.open
  const highPrice = data.high
  const lowPrice = data.low
  const volume = data.volume
  //console.log("Latest Price Date "+tradingDate+":"+closingPrice+":"+openPrice+":"+highPrice+":"+lowPrice+":"+volume)
  return {
    s:'ok',
    t:[newDate],
    c:[closingPrice],
    o:[openPrice],
    h:[highPrice],
    l:[lowPrice],
    v:[volume]
  }
}

function buildDataForTradingView(data)
{
  const dateTimeArray = data.map(row=>{
    const tradingDate = new Date(row.dataValues.tradingDate)
    const year = tradingDate.getUTCFullYear()
    const month = tradingDate.getUTCMonth()
    const day= tradingDate.getUTCDate()
    const newDate = Date.UTC(year,month,day,0,0,0,0)
    return (newDate/1000)
  })
  const closingPriceArray = data.map(row=>{
    return row.dataValues.closingPrice
  })
  const openPriceArray = data.map(row=>{
    return row.dataValues.open
  })
  const highPriceArray = data.map(row=>{
    return row.dataValues.high
  })
  const lowPiceArray = data.map(row=>{
    return row.dataValues.low
  })
  const volumeArray = data.map(row=>{
    return row.dataValues.volume
  })
  return{
    s:'ok',
    t:dateTimeArray,
    c:closingPriceArray,
    o:openPriceArray,
    h:highPriceArray,
    l:lowPiceArray,
    v:volumeArray
  }
}

function buildDataForIndicesLive(data){
  
  console.log(data.length)
  const dataCopy = data.slice()
  const dataSortByTimeStampDesc = data.sort((obj1,obj2)=>{return obj2.dataValues.timestamp-obj1.dataValues.timestamp})
  const dataSortByValueDesc = dataCopy.sort((obj1,obj2)=>{return obj2.dataValues.value-obj1.dataValues.value})

  const dateTime = dataSortByTimeStampDesc[0].dataValues.timestamp
  
  const tradingDate = new Date(dateTime)
  const year = tradingDate.getUTCFullYear()
  const month = tradingDate.getUTCMonth()
  const day= tradingDate.getUTCDate()
  const newDate = (Date.UTC(year,month,day,0,0,0,0)/1000)

  const close = dataSortByTimeStampDesc[0].dataValues.value
  const open =  dataSortByTimeStampDesc[data.length-1].dataValues.value
  const low =  dataSortByValueDesc[data.length-1].dataValues.value
  const high =  dataSortByValueDesc[0].dataValues.value
  const volume = dataSortByTimeStampDesc[0].dataValues.turnover

  return{
    s:'ok',
    t:[newDate],
    c:[close],
    o:[open],
    h:[high],
    l:[low],
    v:[volume]
  }

}

function combinePriceData(history,live){
  const dateFromLive = live.t[0]
  const timeDataArray = history.t.slice()
  const lastDateFromHistory = timeDataArray.sort((a,b)=>b-a)[0]
  //console.log("First "+history.t.length)
  
  if(dateFromLive>lastDateFromHistory){
    history.t.push(live.t[0])
    history.c.push(live.c[0])
    history.o.push(live.o[0])
    history.h.push(live.h[0])
    history.l.push(live.l[0])
    history.v.push(live.v[0])
    //console.log("Live is higher")
  }
  //console.log("Second "+history.t.length)
  return history;
 

}

//Bars
router.get('/history', (req, res, next) => {
  const symbol = req.query.symbol
  const resolution = req.query.resolution
  const from = req.query.from
  const to = req.query.to
  //Actual
  const fromDate = new Date(Number(from)*1000)
  const toDate = new Date(Number(to)*1000)

  if(symbol.includes("index")){
  const indexSymbol = symbol.split("_")[0]  
  console.log("Search " +indexSymbol)
  
  const indicesData = IndicesData.findAll({
    where:{
      indicesName:indexSymbol,
      tradingDate:{$between:[fromDate,toDate]}
    }
  }).then(data=>{
    return data
  })
    
  const indicesLatestPrice  = IndicesDataOneDayLive.findAll({
    where:{
      indicesName:indexSymbol,
      timestamp:{$lte:toDate}
    }
  }).then(data=>{
    return data
  })
  
  Promise.all([indicesData,indicesLatestPrice]).then((combinedData)=>{
    if(combinedData[0].length>0 && combinedData[1].length>0){
      console.log(combinedData[0].length+":"+combinedData[1].length)
      const finalData = combinePriceData(buildDataForTradingView(combinedData[0]),buildDataForIndicesLive(combinedData[1]))
      res.send(JSON.stringify(finalData))
    }else if(combinedData[0].length>0 && combinedData[1].length==0){
      const indicesData = buildDataForTradingView(combinedData[0])
      res.send(JSON.stringify(indicesData))
    }else if(combinedData[1].length>1 && combinedData[0].length==0){
      const indicesLiveData = buildDataForIndicesLive(combinedData[1])
      res.send(JSON.stringify(indicesLiveData))
    }else{
      const noDataResponse = {s:"no_data"}
      res.send(JSON.stringify(noDataResponse))
    }
  }).catch(()=>{
    const noDataResponse = {s:"no_data"}
    res.send(JSON.stringify(noDataResponse))
  })

  }else{
    const stockData = StockData.findAll({
      where: {
        ticker: symbol,
        tradingDate: {$between:[fromDate,toDate]}
      }
    }).then(data => {
      return data
    })
  
    const latestPrice = LatestPriceLive.findAll({
      where:{
        ticker:symbol,
        timestamp:{$lte:toDate}
      }
    }).then(data=>{
      return data
    })
  
    Promise.all([stockData,latestPrice]).then((combinedData)=>{
        
        const stockData = combinedData[0]
        const latestPriceData = combinedData[1]
        
      // if(stockData.length>0){
      //   res.send(JSON.stringify(buildDataForTradingView(stockData)))
      // }else if(latestPriceData.length>0){
      //   res.send(JSON.stringify(buildDataForLatest(latestPriceData)))
      // }
      // else{
      //   const noDataResponse = {s:"no_data"}
      //   res.send(JSON.stringify(noDataResponse))
      // }
      if(combinedData[0].length>0 && combinedData[1].length>0){
        const finalData = combinePriceData(buildDataForTradingView(combinedData[0]),buildDataForLatest(combinedData[1]))
        res.send(JSON.stringify(finalData))
      }else if(combinedData[0].length>0 && combinedData[1].length==0){
        res.send(JSON.stringify(buildDataForTradingView(stockData)))
      }else if(combinedData[1].length>0 && combinedData[0].length==0){
        res.send(JSON.stringify(buildDataForLatest(latestPriceData)))
      }else{
        const noDataResponse = {s:"no_data"}
        res.send(JSON.stringify(noDataResponse))
      }

    }).catch(()=>{
      const noDataResponse = {s:"no_data"}
      res.send(JSON.stringify(noDataResponse))
    })
  }


})


//Marks
//Only if data feed sent supports_marks:true
router.get('/marks', (req, res, next) => {
  const symbol = req.query.symbol
  const resolution = req.query.resolution
  const from = req.query.from
  const to = req.query.to

  const url = 'https://demo_feed.tradingview.com/marks?symbol=' + symbol + "&resolution=" + resolution + '&from=' + from + "&to=" + to
  //'https://demo_feed.tradingview.com/history?symbol=AAPL&resolution=D&from=1510902195&to=1542006255'
  https.get(url, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      //console.log(data);
      res.send(data)
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
    res.send("Error")
  });

})
//Timescale marks
//Only if data feed sent supports_timescale_marks: true
router.get('/timescale_marks', (req, res, next) => {
  const symbol = req.query.symbol
  const resolution = req.query.resolution
  const from = req.query.from
  const to = req.query.to

  const url = 'https://demo_feed.tradingview.com/timescale_marks?symbol=' + symbol + "&resolution=" + resolution + '&from=' + from + "&to=" + to
  https.get(url, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      //console.log(data);
      res.send(data)
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
    res.send("Error")
  });

})
//Time
router.get('/time', (req, res, next) => {
  const serverTime = Math.floor(new Date() / 1000)
  res.send(serverTime.toString())
})
//Quotes
router.get('/quotes', (req, res, next) => {
  const symbol = req.query.symbols
  const url = 'https://demo_feed.tradingview.com/quotes?symbols=' + symbol
  //console.log(url)
  https.get(url, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      //console.log(data);
      res.send(data)
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
    res.send("Error")
  });

})

module.exports = router
