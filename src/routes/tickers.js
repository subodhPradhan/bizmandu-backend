/**
 * Created by admin to 2018/11/4.
 */
const router = require('express').Router()
const {TickersService} = require('../services')
const Utils = require('../common/util')

router.get('/all', (req, res, next) => {
  TickersService.getAllTicker((err, result) => {
    if (!err) {
      if (result.length > 0) {
        result = result.map((row) =>
            (Utils.removeNull({
              ticker: row.ticker,
              companyName: row.companyName,
              sector: row.sector
            })))
      }
      Utils.response(200, 'Ticker List', result, '', res)
    } else {
      Utils.response(500, 'Ticker List', [], 'Internal Server Error', res)
    }
  })
})

module.exports = router
