/**
 * Created by admin on 2018/11/4.
 */
const router = require('express').Router()

router.use(function (req, res, next) {
  console.log('base route =>', req.originalUrl)
  next()
})

// defined api list
router.use('/announcement', require('./announcement'))
router.use('/overview', require('./marketOverview'))
router.use('/stockPrice', require('./stockPrice'))
router.use('/tearsheet', require('./tearsheet'))
router.use('/tickers', require('./tickers'))
router.use('/tradingView', require('./tradingView'))
router.use('/tradingViewSystemxLite',require("./tradingViewSystemxLite"))
router.use('/overall/summary', require('./overallSummary'))
// ...

module.exports = router
