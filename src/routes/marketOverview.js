/**
 * Created by admin to 2018/11/4.
 */
const router = require('express').Router()
const {MarketOverviewService, StockPriceService} = require('../services')
const Utils = require('../common/util')

router.get('/activeBrokers', (req, res, next) => {
  if (req.query['count'] !== null && req.query['count'] !== undefined) {
    MarketOverviewService.ActiveBroker.get(req.query['count'], (err, result) => {
      if (!err) {
        if (result.length > 0) {
          result = result.map((row) =>
            ({
              broker: row.broker,
              totalAmount: row.totalAmount,
              topSell: row.topSell,
              topBuy: row.topBuy
            }))
        }
        Utils.response(200, 'Active Brokers', result, '', res)
      } else {
        Utils.response(500, 'Active Brokers', [], 'Internal Server Error', res)
      }
    })
  } else {
    MarketOverviewService.ActiveBroker.getAll((err, result) => {
      if (!err) {
        if (result.length > 0) {
          result = result.map((row) =>
            ({
              broker: row.broker,
              totalAmount: row.totalAmount,
              topSell: row.topSell,
              topBuy: row.topBuy
            }))
        }
        Utils.response(200, 'Market Overview Active Brokers', result, '', res)
      } else {
        Utils.response(500, 'Market Overview Active Brokers', [], 'Internal Server Error', res)
      }
    })
  }
})

router.get('/events', (req, res, next) => {
  MarketOverviewService.Events.getAll((err, result) => {
    if (!err) {
      if (result.length > 0) {
        result = result.map((row) =>
          ({
            ticker: row.ticker,
            detail: row.detail
          }))
      }
      Utils.response(200, 'Market Overview Events', result, '', res)
    } else {
      Utils.response(500, 'Market Overview Events', [], 'Internal Server Error', res)
    }
  })
})

router.get('/highestTurnover', (req, res, next) => {
  if (req.query['count'] !== null && req.query['count'] !== undefined) {
    StockPriceService.StockPrice.getHighestTurnover(req.query['count'], (err, result) => {
      if (!err) {
        Utils.response(200, 'Market Overview Highest Turnover', result, '', res)
      } else {
        Utils.response(500, 'Market Overview Highest Turnover', [], 'Internal Server Error', res)
      }
    })
  } else {
    StockPriceService.StockPrice.getHighestTurnoverAll((err, result) => {
      if (!err) {
        Utils.response(200, 'Market Overview Highest Turnover', result, '', res)
      } else {
        Utils.response(500, 'Market Overview Highest Turnover', [], 'Internal Server Error', res)
      }
    })
  }
})

router.get('/indices', (req, res, next) => {
  MarketOverviewService.IndicesSummary.getAll((err, result) => {
    if (!err) {
      if (result.length > 0) {
        result = result.map((row) =>
          ({
            indicesName: row.indices,
            label: Utils.getCorrespondingIndicesName(row.indices),
            turnover: row.volume,
            current: row.current,
            pointsChange: row.pointChange,
            percentageChange: row.percentageChange,
            direction: row.percentageChange > 0 ? 'positive' : 'negative'
          })
        )
      }
      Utils.response(200, 'Market Overview Indices', result, '', res)
    } else {
      Utils.response(500, 'Market Overview Indices', [], 'Internal Server Error', res)
    }
  })
})

router.get('/summary', (req, res, next) => {
  if (req.query['type'] !== null && req.query['type'] !== undefined) {
    MarketOverviewService.IndicesSummary.get(req.query['type'].toUpperCase(), (err, result) => {
      if (result !== null) {
        if (!err) {
          result = {
            current: result.current,
            pointChange: result.pointChange,
            percentageChange: result.percentageChange,
            volume: result.volume,
            advancers: result.advancers,
            decliners: result.decliners
          }
        }
        Utils.response(200, 'Market Overview Summary', result, '', res)
      } else {
        Utils.response(401, 'Market Overview Summary', [], 'Invalid Param', res)
      }
    })
  } else {
    Utils.response(500, 'Market Overview Summary', [], 'Query parameter not present.', res)
  }
})

router.get('/topGainers', (req, res, next) => {
  if (req.query['count'] !== null && req.query['count'] !== undefined) {
    StockPriceService.StockPrice.getTopGainers(req.query['count'], (err, result) => {
      if (!err) {
        Utils.response(200, 'Market Overview TopGainers', result, '', res)
      } else {
        Utils.response(500, 'Market Overview TopGainers', [], 'Internal Server Error', res)
      }
    })
  } else {
    StockPriceService.StockPrice.getTopGainersAll((err, result) => {
      if (!err) {
        Utils.response(200, 'Market Overview TopGainers', result, '', res)
      } else {
        Utils.response(500, 'Market Overview TopGainers', [], 'Internal Server Error', res)
      }
    })
  }
})

router.get('/topLosers', (req, res, next) => {
  if (req.query['count'] !== null && req.query['count'] !== undefined) {
    StockPriceService.StockPrice.getTopLosers(req.query['count'], (err, result) => {
      if (!err) {
        Utils.response(200, 'Market Overview Top Losers', result, '', res)
      } else {
        Utils.response(500, 'Market Overview Top Losers', [], 'Internal Server Error', res)
      }
    })
  } else {
    StockPriceService.StockPrice.getTopLosersAll((err, result) => {
      if (!err) {
        Utils.response(200, 'Market Overview Top Losers', result, '', res)
      } else {
        Utils.response(500, 'Market Overview Top Losers', [], 'Internal Server Error', res)
      }
    })
  }
})

router.get('/indicesData', (req, res, next) => {
  if (req.query['indicesName'] !== null && req.query['indicesName'] !== undefined && req.query['period'] !== null && req.query['period'] !== undefined) {
    const indicesName = req.query['indicesName'] === 'hydropower' ? 'HydroPower' : Utils.getCorrespondingIndicesName(req.query['indicesName'])
    switch (req.query['period']) {
      case '1d': {
        MarketOverviewService.IndicesPriceVolume.getOneDayData(indicesName, (err, result) => {
          if (!err) {
            Utils.response(200, 'Market Overview Indices Data', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Market Overview Indices Data', [], 'Internal Server Error', res)
          }
        })
        break
      }
      case '1m': {
        MarketOverviewService.IndicesPriceVolume.getOneMonthData(indicesName, (err, result) => {
          console.log(err)
          if (!err) {
            Utils.response(200, 'Market Overview Indices Data', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Market Overview Indices Data', [], 'Internal Server Error', res)
          }
        })
        break
      }
      case '1y': {
        MarketOverviewService.IndicesPriceVolume.getOneYearData(indicesName, (err, result) => {
          if (!err) {
            Utils.response(200, 'Market Overview Indices Data', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Market Overview Indices Data', [], 'Internal Server Error', res)
          }
        })
        break
      }
      case '5y': {
        MarketOverviewService.IndicesPriceVolume.getFiveYearData(indicesName, (err, result) => {
          if (!err) {
            Utils.response(200, 'Market Overview Indices Data', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Market Overview Indices Data', [], 'Internal Server Error', res)
          }
        })
        break
      }
      default:
        Utils.response(401, 'Market Overview Indices Data', [], 'Query parameter invalid', res)
    }
  } else {
    Utils.response(500, 'Market Overview Indices Data', [], 'Query parameter not present.', res)
  }
})

module.exports = router
