/**
 * Created by admin to 2018/11/4.
 */
const router = require('express').Router()
const {AnnouncementService, TearsheetService, TickersService} = require('../services')
const Utils = require('../common/util')

router.get('/announcement', (req, res, next) => {
  if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
    AnnouncementService.Announcement.getAllForTicker(req.query['tkr'], (err, result) => {
      if (!err) {
        if (result.length > 0) {
          result = result.map((row) =>
            (Utils.removeNull({
              id: row.id,
              ticker: row.ticker,
              company: row.companyName,
              inputDate: row.inputDate,
              eventDate: row.eventDate,
              fiscalYear: row.fiscalYear,
              details: row.details,
              agenda: row.agenda,
              openDate: row.openDate,
              closeDate: row.closeDate,
              bookCloseDate: row.bookCloseDate,
              type: row.announcementType,
              allotmentDate: row.allotmentDate,
              shareRegistrar: row.shareRegistrar,
              venue: row.venue,
              time: row.time
            })))
        }
        Utils.response(200, 'Announcement', result, '', res)
      } else {
        Utils.response(500, 'Announcement', [], 'Internal Server Error', res)
      }
    })
  } else {
    Utils.response(500, 'Announcement', [], 'Query parameter not present.', res)
  }
})

router.get('/priceVolChart', (req, res, next) => {
  if (req.query['tkr'] !== null && req.query['tkr'] !== undefined && req.query['period'] !== null && req.query['period'] !== undefined) {
    switch (req.query['period']) {
      case '1d': {
        TearsheetService.StockPriceVolume.getOneDayData(req.query['tkr'], (err, result) => {
          if (!err) {
            Utils.response(200, 'Company Price Volume Chart', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Company Price Volume Chart', [], 'Internal Server Error', res)
          }
        })
        break
      }
      case '6m': {
        TearsheetService.StockPriceVolume.getSixMonthData(req.query['tkr'], (err, result) => {
          if (!err) {
            Utils.response(200, 'Company Price Volume Chart', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Company Price Volume Chart', [], 'Internal Server Error', res)
          }
        })
        break
      }
      case '1y': {
        TearsheetService.StockPriceVolume.getOneYearData(req.query['tkr'], (err, result) => {
          if (!err) {
            Utils.response(200, 'Company Price Volume Chart', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Company Price Volume Chart', [], 'Internal Server Error', res)
          }
        })
        break
      }
      case '5y': {
        TearsheetService.StockPriceVolume.getFiveYearData(req.query['tkr'], (err, result) => {
          if (!err) {
            Utils.response(200, 'Company Price Volume Chart', Utils.removeNull(result), '', res)
          } else {
            Utils.response(500, 'Company Price Volume Chart', [], 'Internal Server Error', res)
          }
        })
        break
      }
      default:
        Utils.response(401, 'Company Price Volume Chart', [], 'Query parameter invalid', res)
    }
  } else {
    Utils.response(500, 'Company Price Volume Chart', [], 'Query parameter not present.', res)
  }
})

router.get('/dividend', (req, res, next) => {
  if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
    TearsheetService.Tearsheet_Dividend.get(req.query['tkr'], (err, result) => {
      if (!err) {
        result = (Utils.removeNull({
          ticker: result.ticker,
          dividend: JSON.parse(result.dividend).sort((a, b) => a.year - b.year),
          rights: JSON.parse(result.rights)
        }))
        Utils.response(200, 'Dividend', result, '', res)
      } else {
        Utils.response(401, 'Dividend', [], 'No Record Found', res)
      }
    })
  } else {
    Utils.response(500, 'Dividend', [], 'Query parameter not present.', res)
  }
})

router.get('/financial/keyStats', (req, res, next) => {
  if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
    TearsheetService.Financial_Keystat.get(req.query['tkr'], (err, result) => {
      if (!err) {
        result = (Utils.removeNull({
          ticker: result.ticker,
          data: JSON.parse(result.data.replace(new RegExp('\'', 'g'), '"')),
          format: JSON.parse(result.format.replace(new RegExp('\'', 'g'), '"'))
        }))
        Utils.response(200, 'Financial Key Stats', result, '', res)
      } else {
        Utils.response(401, 'Financial Key Stats', [], 'No Record Found', res)
      }
    })
  } else {
    Utils.response(500, 'Financial Key Stats', [], 'Query parameter not present.', res)
  }
})

router.get('/financial/balanceSheet', (req, res, next) => {
  if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
    TearsheetService.Tearsheet_Financial_Balancesheet.get(req.query['tkr'], (err, result) => {
      if (!err) {
        result = (Utils.removeNull({
          ticker: result.ticker,
          data: JSON.parse(result.data.replace(new RegExp('\'', 'g'), '"')),
          format: JSON.parse(result.format.replace(new RegExp('\'', 'g'), '"'))
        }))
        Utils.response(200, 'Financial Balance Sheet', result, '', res)
      } else {
        Utils.response(401, 'Financial Balance Sheet', [], 'No Record Found', res)
      }
    })
  } else {
    Utils.response(500, 'Financial Balance Sheet', [], 'Query parameter not present.', res)
  }
})

router.get('/financial/incomeStatement', (req, res, next) => {
  if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
    TearsheetService.Tearsheet_Financial_Incomestatement.get(req.query['tkr'], (err, result) => {
      if (!err) {
        result = (Utils.removeNull({
          ticker: result.ticker,
          data: JSON.parse(result.data.replace(new RegExp('\'', 'g'), '"')),
          format: JSON.parse(result.format.replace(new RegExp('\'', 'g'), '"'))
        }))
        Utils.response(200, 'Financial Income Statement', result, '', res)
      } else {
        Utils.response(401, 'Financial Income Statement', [], 'No Record Found', res)
      }
    })
  } else {
    Utils.response(500, 'Financial Income Statement', [], 'Query parameter not present.', res)
  }
})

router.get('/header', (req, res, next) => {
  if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
    TickersService.get(req.query['tkr'], (error, resultT) => {
      TearsheetService.Header.get(req.query['tkr'], (err, result) => {
        let companyNameTemp = null
        if (!error) { companyNameTemp = resultT.companyName }
        if (!err) {
          result = (Utils.removeNull({
            ticker: result.ticker,
            company: companyNameTemp,
            latestPrice: result.latestPrice,
            pointChange: result.pointChange,
            percentageChange: result.percentageChange,
            timestamp: result.timestamp,
            wtAvgPrice: result.wtAvgPrice,
            sharesTraded: result.sharesTraded,
            volume: result.volume,
            mktCap: result.mktCap
          }))
          Utils.response(200, 'Tearsheet Live Header', result, '', res)
        } else {
          Utils.response(401, 'Tearsheet Live Header', [], 'No Record Found', res)
        }
      })
    })
  } else {
    Utils.response(500, 'Tearsheet Live Header', [], 'Query parameter not present.', res)
  }
})

router.get('/summary', (req, res, next) => {
  TearsheetService.Tearsheet.get(req.query['tkr'], (keyFinancial, summary) => {
    if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
      let temp = []
      let bvps = null
      if (keyFinancial !== null) {
        if (keyFinancial.current !== null) {
          let current = JSON.parse(keyFinancial.current.replace(new RegExp('\'', 'g'), '"'))
          temp.push(current)
          bvps = current['bvps'] !== undefined ? current['bvps'] : null
        }
        if (keyFinancial.percentageChangeVsLastQuarter !== null) { temp.push(JSON.parse(keyFinancial.percentageChangeVsLastQuarter.replace(new RegExp('\'', 'g'), '"'))) }
        if (keyFinancial.percentageChangeVsPrevYear !== null) { temp.push(JSON.parse(keyFinancial.percentageChangeVsPrevYear.replace(new RegExp('\'', 'g'), '"'))) }
      }
      keyFinancial = (keyFinancial !== undefined && keyFinancial !== null) ? (Utils.removeNull({
        ticker: keyFinancial.ticker,
        year: keyFinancial.year,
        quarter: keyFinancial.quarter,
        data: temp
      })) : ({})
      summary = (summary !== undefined && summary !== null) ? (Utils.removeNull({
        ticker: summary.ticker,
        open: summary.open,
        avgVolume: summary.avgVolume,
        daysHigh: summary.daysHigh,
        daysLow: summary.daysLow,
        fiftyTwoWeekHigh: summary.fiftyTwoWeekHigh,
        fiftyTwoWeekLow: summary.fiftyTwoWeekLow,
        listedShares: summary.listedShares,
        mktCap: summary.mktCap,
        epsDiluted: summary.epsDiluted,
        peDiluted: summary.peDiluted,
        bvps: bvps,
        beta: summary.beta
      })) : ({})
      Utils.response(200, 'Tearsheet Summary', {keyFinancial: keyFinancial, summary: summary}, '', res)
    } else {
      Utils.response(500, 'Tearsheet Summary', [], 'Query parameter not present.', res)
    }
  })
})

router.get('/summaryMf', (req, res, next) => {
  TearsheetService.TearsheetMf.get(req.query['tkr'], (result) => {
    if (req.query['tkr'] !== null && req.query['tkr'] !== undefined) {
      result = result !== undefined ? (Utils.removeNull({
        ticker: result.ticker,
        open: result.open,
        avgVolume: result.avgVolume,
        daysHigh: result.daysHigh,
        daysLow: result.daysLow,
        fiftyTwoWeekHigh: result.fiftyTwoWeekHigh,
        fiftyTwoWeekLow: result.fiftyTwoWeekLow,
        aum: result.aum,
        weeklyNav: result.weeklyNav,
        monthlyNav: result.monthlyNav,
        priceVsNav: result.priceVsNav,
        fundBeta: result.fundBeta,
        totalSectorsInvested: result.totalSectorsInvested,
        totalSharesHeld: result.totalSharesHeld,
        totalCompaniesHeld: result.totalCompaniesHeld,
        sector: JSON.parse(result.sector),
        topStockHoldings: JSON.parse(result.topStockHoldings),
        topStockBought: JSON.parse(result.topStockBought),
        topStockSold: JSON.parse(result.topStockSold)
      })) : ({})

      Utils.response(200, 'Tearsheet Mutual Fund', {summary: result}, '', res)
    } else {
      Utils.response(500, 'Tearsheet Mutual Fund', [], 'Query parameter not present.', res)
    }
  })
})

module.exports = router
