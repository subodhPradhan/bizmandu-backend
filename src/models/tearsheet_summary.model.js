/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Tearsheet_Summary', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: 'ticker'
    },
    open: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'open'
    },
    avgVolume: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'avgVolume'
    },
    daysHigh: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'daysHigh'
    },
    daysLow: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'daysLow'
    },
    fiftyTwoWeekHigh: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'fiftyTwoWeekHigh'
    },
    fiftyTwoWeekLow: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'fiftyTwoWeekLow'
    },
    listedShares: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'listedShares'
    },
    mktCap: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'mktCap'
    },
    epsDiluted: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'epsDiluted'
    },
    peDiluted: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'peDiluted'
    },
    bvps: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'bvps'
    },
    beta: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'beta'
    }
  }, {
    tableName: 'tearsheet_summary',
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['ticker']
      }
    ]
  })
}
