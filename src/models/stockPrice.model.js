/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('StockPrice', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: 'ticker'
    },
    ltp: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'ltp'
    },
    ltv: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'ltv'
    },
    pointChange: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'pointChange'
    },
    percentageChange: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'percentageChange'
    },
    open: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'open'
    },
    high: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'high'
    },
    low: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'low'
    },
    volume: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'volume'
    }
  }, {
    tableName: 'stock_price',
    // timestamps: false,
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['ticker']
      }
    ]
  })
}
