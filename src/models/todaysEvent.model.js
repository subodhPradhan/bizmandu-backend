/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('TodaysEvent', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: 'ticker'
    },
    company: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'company'
    },
    inputDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'inputDate'
    },
    eventType: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: 'eventType'
    },
    fiscalYear: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: 'fiscalYear'
    },
    details: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'details'
    },
    shareRegistrar: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'shareRegistrar'
    },
    eventDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'eventDate'
    },
    openDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'openDate'
    },
    closeDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'closeDate'
    },
    bookCloseDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'bookCloseDate'
    },
    allotmentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'allotmentDate'
    },
    venue: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'venue'
    },
    agenda: {
      type: DataTypes.STRING(1000),
      allowNull: true,
      field: 'agenda'
    },
    time: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'time'
    }
  }, {
    tableName: 'todays_event',
    // timestamps: false,
    freezeTableName: true
  })
}
