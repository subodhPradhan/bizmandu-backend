/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Announcement', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: 'ticker'
    },
    companyName: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'companyName'
    },
    inputDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'inputDate'
    },
    eventDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'eventDate'
    },
    fiscalYear: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: 'fiscalYear'
    },
    details: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'details'
    },
    agenda: {
      type: DataTypes.STRING(1000),
      allowNull: true,
      field: 'agenda'
    },
    openDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'openDate'
    },
    closeDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'closeDate'
    },
    bookCloseDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'bookCloseDate'
    },
    announcementType: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: 'announcementType'
    },
    allotmentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'allotmentDate'
    },
    shareRegistrar: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'shareRegistrar'
    },
    venue: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'venue'
    },
    time: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'time'
    }
  }, {
    tableName: 'announcement',
    // timestamps: false,
    freezeTableName: true
  })
}
