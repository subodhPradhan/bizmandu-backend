/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Tearsheet_KeyFinancial', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: 'ticker'
    },
    year: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'year'
    },
    quarter: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'quarter'
    },
    current: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'current'
    },
    percentageChangeVsLastQuarter: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'percentageChangeVsLastQuarter'
    },
    percentageChangeVsPrevYear: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'percentageChangeVsPrevYear'
    }
  }, {
    tableName: 'tearsheet_keyFinancial',
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['ticker']
      }
    ]
  })
}
