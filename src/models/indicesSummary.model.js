/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('IndicesSummary', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    indices: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'indices'
    },
    current: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'current'
    },
    pointChange: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'pointChange'
    },
    percentageChange: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'percentageChange'
    },
    volume: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'volume'
    },
    advancers: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'advancers'
    },
    decliners: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'decliners'
    }
  }, {
    tableName: 'indices_summary',
    freezeTableName: true
  })
}
