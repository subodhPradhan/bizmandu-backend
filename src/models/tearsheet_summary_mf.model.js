/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Tearsheet_Summary_MF', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: 'ticker'
    },
    open: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'open'
    },
    avgVolume: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'avgVolume'
    },
    daysHigh: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'daysHigh'
    },
    daysLow: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'daysLow'
    },
    fiftyTwoWeekHigh: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'fiftyTwoWeekHigh'
    },
    fiftyTwoWeekLow: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'fiftyTwoWeekLow'
    },
    aum: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'aum'
    },
    weeklyNav: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'weeklyNav'
    },
    monthlyNav: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'monthlyNav'
    },
    priceVsNav: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'priceVsNav'
    },
    fundBeta: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'fundBeta'
    },
    totalSectorsInvested: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'totalSectorsInvested'
    },
    totalSharesHeld: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'totalSharesHeld'
    },
    totalCompaniesHeld: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'totalCompaniesHeld'
    },
    sector: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'sector'
    },
    topStockHoldings: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'topStockHoldings'
    },
    topStockBought: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'topStockBought'
    },
    topStockSold: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'topStockSold'
    }
  }, {
    tableName: 'tearsheet_summary_mf',
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['ticker']
      }
    ]
  })
}
