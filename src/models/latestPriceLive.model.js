module.exports = function (sequelize, DataTypes) {
  return sequelize.define('LatestPriceLive', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(200),
      allowNull: false,
      field: 'ticker'
    },
    transactions: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'transactions'
    },
    high: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'high'
    },
    low: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'low'
    },
    closingPrice: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'closingPrice'
    },
    volume: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'volume'
    },
    amount: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'amount'
    },
    open: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'open'
    },
    dataSource: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'dataSource'
    },
    contractNumber: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'contractNumber'
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'timestamp'
    }
  }, {
    tableName: 'latest_price_live',
    freezeTableName: true,
    timestamps: false
  })
}
