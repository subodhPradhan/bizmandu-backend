module.exports = function (sequelize, DataTypes) {
  return sequelize.define('SubIndicesLiveWithVol', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    sector: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: 'sector'
    },
    value: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      field: 'value'
    },
    turnover: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'turnover'
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'timestamp'
    }
  }, {
    tableName: 'sub_indices_live_with_vol',
    freezeTableName: true,
    timestamps: false
  })
}
