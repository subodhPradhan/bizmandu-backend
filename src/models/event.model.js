/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Event', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: 'ticker'
    },
    detail: {
      type: DataTypes.STRING(250),
      allowNull: true,
      field: 'detail'
    }
  }, {
    tableName: 'event',
    freezeTableName: true
  })
}
