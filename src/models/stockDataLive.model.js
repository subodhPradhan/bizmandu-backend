module.exports = function (sequelize, DataTypes) {
  return sequelize.define('StockDataLive', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(200),
      allowNull: false,
      field: 'ticker'
    },
    closingPrice: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'closingPrice'
    },
    volume: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'volume'
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'timestamp'
    }
  }, {
    tableName: 'stock_data_live',
    freezeTableName: true,
    timestamps: false
  })
}
