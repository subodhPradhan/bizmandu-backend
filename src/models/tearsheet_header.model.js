/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Tearsheet_Header', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: 'ticker'
    },
    pointChange: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'pointChange'
    },
    percentageChange: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'percentageChange'
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'timestamp'
    },
    wtAvgPrice: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'wtAvgPrice'
    },
    sharesTraded: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'sharesTraded'
    },
    volume: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'volume'
    },
    mktCap: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'mktCap'
    },
    latestPrice: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'latestPrice'
    }
  }, {
    tableName: 'tearsheet_header',
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['ticker']
      }
    ]
  })
}
