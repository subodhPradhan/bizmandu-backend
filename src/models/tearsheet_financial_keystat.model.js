/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Tearsheet_Financial_Keystat', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: 'ticker'
    },
    data: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'data'
    },
    format: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'format'
    }
  }, {
    tableName: 'tearsheet_financial_keystat',
    freezeTableName: true
  })
}
