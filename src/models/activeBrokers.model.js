/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('ActiveBroker', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    broker: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'broker'
    },
    totalAmount: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      field: 'totalAmount'
    },
    topSell: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'topSell'
    },
    topBuy: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: 'topBuy'
    }
  }, {
    tableName: 'active_brokers',
    freezeTableName: true
  })
}
