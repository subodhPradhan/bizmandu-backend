module.exports = function (sequelize, DataTypes) {
    return sequelize.define('IndicesNames', {
     id:{
        type: DataTypes.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        field: 'id',
        default: 0
     },
     indicesName: {
        type: DataTypes.STRING(200),
        allowNull: true,
        field: 'indicesname'
      },
    }, {
      tableName: 'indicesNames',
      freezeTableName: true,
      timestamps:false
    })
  }