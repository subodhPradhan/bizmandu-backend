/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Tearsheet_Dividend', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: 'ticker'
    },
    dividend: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'dividend'
    },
    rights: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'rights'
    }
  }, {
    tableName: 'tearsheet_dividend',
    freezeTableName: true
  })
}
