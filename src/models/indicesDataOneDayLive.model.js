module.exports = function (sequelize, DataTypes) {
  return sequelize.define('IndicesDataOneDayLive', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    indicesName: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: 'indicesName'
    },
    value: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'value'
    },
    turnover: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'turnover'
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'timestamp'
    }
  }, {
    tableName: 'indices_data_one_day_live',
    freezeTableName: true,
    timestamps: false
  })
}

