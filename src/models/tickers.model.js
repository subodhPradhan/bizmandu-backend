/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Tickers', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    ticker: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: 'ticker'
    },
    companyName: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: 'companyName'
    },
    sector: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: 'sector'
    }
  }, {
    tableName: 'tickers',
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['ticker']
      }
    ]
  })
}
