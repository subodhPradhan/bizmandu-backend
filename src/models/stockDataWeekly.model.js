module.exports = function (sequelize, DataTypes) {
  return sequelize.define('StockDataWeekly', {
    idstock_data_weekly: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'idstock_data_weekly',
      default: 0
    },
    companyFullName: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'companyFullName'
    },
    ticker: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: 'ticker'
    },
    transactions: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'transactions'
    },
    high: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'high'
    },
    low: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'low'
    },
    closingPrice: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'closingPrice'
    },
    volume: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'volume'
    },
    amount: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'amount'
    },
    open: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'open'
    },
    dataSource: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'dataSource'
    },
    tradingDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      field: 'trading_date'
    },
    diffRs: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'diffRs'
    }
  }, {
    tableName: 'stock_data_weekly',
    freezeTableName: true,
    timestamps: false
  })
}
