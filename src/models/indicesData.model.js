module.exports = function (sequelize, DataTypes) {
  return sequelize.define('IndicesData', {
    idindicesData2: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'idindicesData2',
      default: 0
    },
    indicesName: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: 'indicesName'
    },
    open: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'open'
    },
    high: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'high'
    },
    low: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'low'
    },
    closingPrice: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'closingPrice'
    },
    volume: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'volume'
    },
    tradingDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      field: 'trading_date'
    },
    amount: {
      type: DataTypes.DOUBLE(16, 4),
      allowNull: true,
      field: 'amount'
    }
  }, {
    tableName: 'indices_data',
    freezeTableName: true,
    timestamps: false
  })
}
