/**
 * Created by admin on 2018/11/4.
 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('ForexSummary', {
    id: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      field: 'id',
      default: 0
    },
    title: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'title'
    },
    current: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'current'
    },
    pointChange: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'pointChange'
    },
    percentageChange: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      field: 'percentageChange'
    }
  }, {
    tableName: 'forex_summary',
    freezeTableName: true,
    timestamps: false,
    indexes: [
      {
        unique: true,
        fields: ['title']
      }
    ]
  })
}
