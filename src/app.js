/**
 * Created by admin to 2018/11/4.
 */
const express = require('express')
const logger = require('morgan')
// const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')

// import routes
const api = require('./routes/index')

const app = express()

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
// app.use(cookieParser('Why not work'))
// app.use(express.static(path.join(__dirname, 'public')));
const Utils = require('./common/util')

app.use(function (req, res, next) {
  // const origin = req.headers.origin
  /* if (typeof origin === 'undefined') {
    // No Cross Origin redirect
    res.header('Access-Control-Allow-Origin', '*')
  } else if (
    (origin.indexOf('http://localhost')) === 0 ||
    (origin.indexOf('http://172.16.') === 0) ||
    (origin.indexOf('http://202.51.') === 0) ||
    (origin.indexOf('http://domain.com') === 0)
  ) {
    res.header('Access-Control-Allow-Origin', origin)
    res.header('Access-Control-Allow-Credentials', 'true')
  } else {
    res.header('Access-Control-Allow-Origin', 'http://localhost')
    res.header('Access-Control-Allow-Origin', 'http://toggle-corp.gitlab.io')
  } */
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Accept-Encoding, X-Access-Token')

  next()
})

// set baseUrl
app.use('/', api)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  Utils.response(404, [], 'Invalid Url', res)
})

module.exports = app
