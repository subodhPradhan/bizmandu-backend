/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('indices_data_one_day_live', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    indicesName: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    value: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    turnover: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'indices_data_one_day_live',
    freezeTableName: true,
    timestamps:false
  });
};
