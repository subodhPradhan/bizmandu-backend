/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('latestpricelive', {
    ticker: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    transactions: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    high: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    low: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    closingPrice: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    volume: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    amount: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    open: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    dataSource: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    contractNumber: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'latestpricelive',
    freezeTableName: true,
    timestamps:false
  });
};
