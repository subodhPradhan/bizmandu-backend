/**
 * Created by admin on 2018/11/4.
 */
const {
  Event, IndicesSummary, ActiveBroker, IndicesDataOneDayLive, IndicesDataMonthly, IndicesData, IndicesDataWeekly
} = require('../models')
const moment = require('moment')
const Op = require('sequelize').Op

const MarketOverviewService = {
  ActiveBroker: {
    get (count, callback) {
      let tempCount = 10
      try {
        tempCount = parseInt(count, 10)
      } catch (e) {
        console.log(e)
      }
      ActiveBroker.findAll({
        order: [['totalAmount', 'DESC']],
        limit: tempCount
      })
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getAll (callback) {
      ActiveBroker.findAll({order: [['totalAmount', 'DESC']]})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  Events: {
    getAll (callback) {
      Event.findAll()
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  IndicesSummary: {
    get (type, callback) {
      IndicesSummary.findOne({where: {indices: type}})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getAll (callback) {
      IndicesSummary.findAll()
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  IndicesPriceVolume: {
    getOneDayData (indicesName, callback) {
      IndicesDataOneDayLive.findAll({
        where: {indicesName: indicesName},
        attributes: ['value', 'turnover', 'timestamp']
      })
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.value, timeStamp: row.timestamp})), volumeChart: data.map((row) => ({value: row.turnover, timeStamp: row.timestamp}))})))
        .catch((err) => callback(err, null))
    },
    getOneMonthData (indicesName, callback) {
      IndicesData.findAll(
        {
          where: {
            indicesName: indicesName,
            tradingDate: {[Op.gte]: moment().subtract(1, 'months').toDate()}
          },
          attributes: ['closingPrice', 'volume', 'tradingDate']
        }
      )
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.closingPrice, timeStamp: row.tradingDate})), volumeChart: data.map((row) => ({value: row.volume, timeStamp: row.tradingDate}))})))
        .catch((err) => callback(err, null))
    },
    getOneYearData (indicesName, callback) {
      IndicesDataWeekly.findAll(
        {
          where: {
            indicesName: indicesName,
            tradingDate: {[Op.gte]: moment().subtract(1, 'years').toDate()}
          },
          attributes: ['closingPrice', 'volume', 'tradingDate']
        }
      )
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.closingPrice, timeStamp: row.tradingDate})), volumeChart: data.map((row) => ({value: row.volume, timeStamp: row.tradingDate}))})))
        .catch((err) => callback(err, null))
    },
    getFiveYearData (indicesName, callback) {
      IndicesDataMonthly.findAll(
        {
          where: {
            indicesName: indicesName,
            tradingDate: {[Op.gte]: moment().subtract(5, 'years').toDate()}
          },
          attributes: ['closingPrice', 'volume', 'tradingDate']
        }
      )
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.closingPrice, timeStamp: row.tradingDate})), volumeChart: data.map((row) => ({value: row.volume, timeStamp: row.tradingDate}))})))
        .catch((err) => callback(err, null))
    }
  }
}
module.exports = MarketOverviewService
