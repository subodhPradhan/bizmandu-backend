/**
 * Created by admin on 2018/11/4.
 */
const { Tickers } = require('../models')

const TickersService = {
  getAllTicker (callback) {
    Tickers.findAll({where: {}})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
  },
  get (ticker, callback) {
    Tickers.findOne({where: {ticker: ticker}})
      .then((data) => callback(null, data))
      .catch((err) => callback(err, null))
  }
}
module.exports = TickersService
