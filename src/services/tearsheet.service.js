/* eslint-disable no-sequences */
/**
 * Created by admin on 2018/11/4.
 */
const {
  Tearsheet_Header, Tearsheet_Summary_MF, Tearsheet_KeyFinancial, Tearsheet_Summary, Tearsheet_Financial_Keystat, Tearsheet_Financial_Balancesheet, Tearsheet_Financial_Incomestatement, Tearsheet_Dividend,
  StockDataLive, StockDataWeekly, StockDataMonthly
} = require('../models')
const moment = require('moment')
const Op = require('sequelize').Op
const TearsheetService = {
  Financial_Keystat: {
    get (ticker, callback) {
      Tearsheet_Financial_Keystat.findOne({where: {ticker: ticker}})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  Header: {
    get (ticker, callback) {
      Tearsheet_Header.findOne({where: {ticker: ticker}})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  Tearsheet: {
    get (ticker, callback) {
      const keyFinancial = Tearsheet_KeyFinancial.findOne({where: {ticker: ticker}})
        .then((data) => data)
      const summary = Tearsheet_Summary.findOne({where: {ticker: ticker}})
        .then((data) => data)
      Promise.all([keyFinancial, summary])
        .then(function (result) {
          callback(result[0], result[1])
        })
    }
  },
  TearsheetMf: {
    get (ticker, callback) {
      const summary = Tearsheet_Summary_MF.findAll({where: {ticker: ticker}})
        .then((data) => data)
      Promise.all([summary])
        .then(function (result) {
          callback(result[0][0])
        })
    }
  },
  Tearsheet_Financial_Balancesheet: {
    get (ticker, callback) {
      Tearsheet_Financial_Balancesheet.findOne({where: {ticker: ticker}})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  Tearsheet_Financial_Incomestatement: {
    get (ticker, callback) {
      Tearsheet_Financial_Incomestatement.findOne({where: {ticker: ticker}})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  Tearsheet_Dividend: {
    get (ticker, callback) {
      Tearsheet_Dividend.findOne({where: {ticker: ticker}})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  StockPriceVolume: {
    getOneDayData (ticker, callback) {
      StockDataLive.findAll({
        where: {ticker: ticker},
        attributes: ['closingPrice', 'volume', 'timestamp']
      })
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.closingPrice, timeStamp: row.timestamp})), volumeChart: data.map((row) => ({value: row.volume, timeStamp: row.timestamp}))})))
        .catch((err) => callback(err, null))
    },
    getSixMonthData (ticker, callback) {
      StockDataWeekly.findAll({
        where: {
          ticker: ticker,
          tradingDate: {[Op.gte]: moment().subtract(6, 'months').toDate()}
        },
        attributes: ['closingPrice', 'volume', 'tradingDate']
      })
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.closingPrice, timeStamp: row.tradingDate})), volumeChart: data.map((row) => ({value: row.volume, timeStamp: row.tradingDate}))})))
        .catch((err) => callback(err, null))
    },
    getOneYearData (ticker, callback) {
      StockDataMonthly.findAll(
        {
          where: {
            ticker: ticker,
            tradingDate: {[Op.gte]: moment().subtract(1, 'years').toDate()}
          },
          attributes: ['closingPrice', 'volume', 'tradingDate']
        }
      )
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.closingPrice, timeStamp: row.tradingDate})), volumeChart: data.map((row) => ({value: row.volume, timeStamp: row.tradingDate}))})))
        .catch((err) => callback(err, null))
    },
    getFiveYearData (ticker, callback) {
      StockDataMonthly.findAll(
        {
          where: {
            ticker: ticker,
            tradingDate: {[Op.gte]: moment().subtract(5, 'years').toDate()}
          },
          attributes: ['closingPrice', 'volume', 'tradingDate']
        }
      )
        .then((data) => callback(null, ({priceChart: data.map((row) => ({value: row.closingPrice, timeStamp: row.tradingDate})), volumeChart: data.map((row) => ({value: row.volume, timeStamp: row.tradingDate}))})))
        .catch((err) => callback(err, null))
    }
  }
}
module.exports = TearsheetService
