/**
 * Created by admin on 2018/11/4.
 */
const { Announcement, TodaysEvent } = require('../models')
const moment = require('moment')
const Op = require('sequelize').Op

const AnnouncementService = {
  Announcement: {
    getSummary (count, callback) {
      let tempCount = 10
      try {
        tempCount = parseInt(count, 10)
      } catch (e) {
        console.log(e)
      }
      Announcement.findAll({
        order: [['inputDate', 'DESC']],
        limit: tempCount
      })
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getAll (type, callback) {
      switch (type) {
        case 'ipo-fpo': {
          Announcement.findAll({
            order: [['inputDate', 'DESC']],
            where: {
              [Op.or]: [
                {
                  inputDate: {[Op.gte]: moment().subtract(3, 'months').toDate()},
                  announcementType: 'IPO'
                },
                {
                  inputDate: {[Op.gte]: moment().subtract(3, 'months').toDate()},
                  announcementType: 'FPO'
                }
              ]
            }
          })
            .then((data) => callback(null, data))
            .catch((err) => callback(err, null))
          break
        }
        case 'agm-sgm': {
          Announcement.findAll({
            order: [['inputDate', 'DESC']],
            where: {
              [Op.or]: [
                {
                  inputDate: {[Op.gte]: moment().subtract(3, 'months').toDate()},
                  announcementType: 'AGM'
                },
                {
                  inputDate: {[Op.gte]: moment().subtract(3, 'months').toDate()},
                  announcementType: 'SGM'
                }
              ]
            }
          })
            .then((data) => callback(null, data))
            .catch((err) => callback(err, null))
          break
        }
        case 'nepse-circular':
        case 'auction':
        case 'right-share':
        case 'dividend': {
          type = type === 'right-share' ? 'Right Shares' : type
          type = type === 'nepse-circular' ? 'Nepse Circular' : type
          Announcement.findAll({
            order: [['inputDate', 'DESC']],
            where: {
              inputDate: {[Op.gte]: moment().subtract(3, 'months').toDate()},
              announcementType: type
            }
          })
            .then((data) => callback(null, data))
            .catch((err) => callback(err, null))
          break
        }
      }
    },
    getAllForTicker (ticker, callback) {
      Announcement.findAll({
        order: [['inputDate', 'DESC']],
        where: {
          ticker: ticker
        }
      })
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  },
  TodaysEvent: {
    getAll (callback) {
      TodaysEvent.findAll()
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    }
  }
}
module.exports = AnnouncementService
