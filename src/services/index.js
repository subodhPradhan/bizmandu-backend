/**
 * Created by admin on 2018/11/4.
 */
module.exports = {
  MarketOverviewService: require('./marketOverview.service'),
  StockPriceService: require('./stockPrice.service'),
  AnnouncementService: require('./announcement.service'),
  TearsheetService: require('./tearsheet.service'),
  TickersService: require('./tickers.service'),
  ForexSummaryService: require('./forexSummary.service'),
}
