/**
 * Created by admin on 2018/11/4.
 */
const {
  ForexSummary
} = require('../models')

const ForexSummaryService = {
  getAll (callback) {
    ForexSummary.findAll({attributes: ['title', 'current', 'pointChange', 'percentageChange']})
      .then((data) => callback(null, (data.map((row) => ({title: row.title, current: row.current, pointChange: row.pointChange, percentageChange: row.percentageChange})))))
      .catch((err) => callback(err, null))
  }
}
module.exports = ForexSummaryService
