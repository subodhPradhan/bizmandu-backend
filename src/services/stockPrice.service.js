/**
 * Created by admin on 2018/11/4.
 */
const { StockPrice } = require('../models')
const Op = require('sequelize').Op

const StockPriceService = {
  StockPrice: {
    getAll (callback) {
      StockPrice.findAll()
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getAllPriceList (callback) {
      StockPrice.findAll({attributes: ['id', 'ticker', 'ltp', 'percentageChange']})
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getTopGainersAll (callback) {
      StockPrice.findAll({
        where: {percentageChange: {[Op.gt]: 0}},
        attributes: ['ticker', 'ltp', 'percentageChange', 'pointChange'],
        order: [['percentageChange', 'DESC']]
      })
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getTopGainers (count, callback) {
      let tempCount = 10
      try {
        tempCount = parseInt(count, 10)
      } catch (e) {
        console.log(e)
      }
      StockPrice.findAll({
        where: {percentageChange: {[Op.gt]: 0}},
        attributes: ['ticker', 'ltp', 'percentageChange', 'pointChange'],
        order: [['percentageChange', 'DESC']],
        limit: tempCount
      })
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getTopLosersAll (callback) {
      StockPrice.findAll({
        where: {percentageChange: {[Op.lt]: 0}},
        attributes: ['ticker', 'ltp', 'percentageChange', 'pointChange'],
        order: [['percentageChange', 'ASC']]
      })
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getTopLosers (count, callback) {
      let tempCount = 10
      try {
        tempCount = parseInt(count, 10)
      } catch (e) {
        console.log(e)
      }
      StockPrice.findAll({
        where: {percentageChange: {[Op.lt]: 0}},
        attributes: ['ticker', 'ltp', 'percentageChange', 'pointChange'],
        order: [['percentageChange', 'ASC']],
        limit: tempCount
      })
        .then((data) => callback(null, data))
        .catch((err) => callback(err, null))
    },
    getHighestTurnoverAll (callback) {
      StockPrice.findAll({
        attributes: ['ticker', 'ltp', 'volume']
      })
        .then((data) => callback(null, data.map((row) => ({ticker: row.ticker, ltp: row.ltp, turnover: row.ltp * row.volume})).sort((a, b) => a.turnover - b.turnover).reverse()))
        .catch((err) => callback(err, null))
    },
    getHighestTurnover (count, callback) {
      let tempCount = 10
      try {
        tempCount = parseInt(count, 10)
      } catch (e) {
        console.log(e)
      }
      StockPrice.findAll({
        attributes: ['ticker', 'ltp', 'volume']
      })
        .then((data) => callback(null, data.map((row) => ({ticker: row.ticker, ltp: row.ltp, turnover: row.ltp * row.volume})).sort((a, b) => a.turnover - b.turnover).reverse().slice(0, tempCount)))
        .catch((err) => callback(err, null))
    }
  }
}
module.exports = StockPriceService
