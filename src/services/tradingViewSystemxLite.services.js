

var {indicesNames,stocksymbolsforsearchbox,stock_data,indicesdata,stock_data_adjusted} = require('../models_systemx')
var {latestpricelive,indices_data_one_day_live} = require('../models_livetrading')


  module.exports = {
    stockData: function(symbol,fromDate,toDate){

    const sd = stock_data.findAll({
        where: {
          ticker: symbol,
          trading_date: {$between:[fromDate,toDate]}
        }
      }).then(data => {
        return data
      })
      return sd
  },
  stockDataAdjusted:function(symbol,fromDate,toDate){
    
   const sd = stock_data_adjusted.findAll({
        where: {
          ticker: symbol,
          trading_date: {$between:[fromDate,toDate]}
        }
      }).then(data => {
        return data
      })
    return sd

  }
}