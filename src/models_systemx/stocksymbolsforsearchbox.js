/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stocksymbolsforsearchbox', {
    idstocksymbols2: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    stock_full_name: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    StockSymbol: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    Sector: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    SectorShortForm: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    SmtmSector: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    SmtmSectorShortForm: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    FirstTradingDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    DataReady: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    ParentTicker: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    Type: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    Status: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    NepListShares: {
      type: "DOUBLE",
      allowNull: true
    },
    Nepseranking: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    Trading: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    tickerToSector: {
      type: DataTypes.STRING(400),
      allowNull: false
    }
  }, {
    tableName: 'stocksymbolsforsearchbox',
    freezeTableName: true,
    timestamps:false
  });
};
