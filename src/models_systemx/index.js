/**
////sequelize-auto -o "./src/models_systemx" -d slickmodel -h localhost -u root -p 3306 -x root -e mysql

* Created by admin on 2018/11/4.
 */
'use strict'

const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
/* ------- Change username according to yours ------ */
console.log('Current User Name ' + process.env.USER)
const env = process.env.USER === 'bihan' ? 'development' : 'production'
const config = require(path.join(__dirname, '..', 'config', 'configSystemx.json'))[env]
let sequelize = process.env.DATABASE_URL ? (
  new Sequelize(process.env.DATABASE_URL)
) : (
  new Sequelize(config.database, config.username, config.password, config.writeConnection)
)

let db = {}

fs
  .readdirSync(__dirname)
  .filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js')
  })
  .forEach(function (file) {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(function (modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
