/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('indicesdata', {
    idindicesData2: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    indices_name: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    open: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    high: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    low: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    closingPrice: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    volume: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    trading_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    amount: {
      type: "DOUBLE(16,4)",
      allowNull: true
    }
  }, {
    tableName: 'indicesdata',
    freezeTableName: true,
    timestamps:false
  });
};
