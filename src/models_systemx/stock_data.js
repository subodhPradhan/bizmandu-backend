/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stock_data', {
    idstock_data: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    company_full_name: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    ticker: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    transactions: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    high: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    low: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    closingPrice: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    volume: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    amount: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    open: {
      type: "DOUBLE(16,4)",
      allowNull: true
    },
    dataSource: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    trading_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    diff_rs: {
      type: "DOUBLE(16,4)",
      allowNull: true
    }
  }, {
    tableName: 'stock_data',
    freezeTableName: true,
    timestamps:false
  });
};
